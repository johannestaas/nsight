nsight
======

Determine services on a server and possible pivots for pentesting

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ nsight

Use --help/-h to view info on the arguments::

    $ nsight --help

Release Notes
-------------

:0.0.1:
    Project created
